use std::io;
use thiserror::Error;

/// Агрегирует в себе возможные ошибки приложения в формате `CutterError`.
#[derive(Debug, Error)]
pub enum CutterError {
    #[error(transparent)]
    ImagePreview(#[from] image::ImageError),
    #[error(transparent)]
    Io(#[from] io::Error),
    #[error(transparent)]
    Request(#[from] reqwest::Error),
    #[error(transparent)]
    MongoDBError(#[from] mongodb::error::Error),
    #[error("другая ошибка: {0}")]
    OtherError(String),
}
