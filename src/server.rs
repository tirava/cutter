//! Модуль для запуска web-сервера.
use crate::prelude::{get_health_check, get_image_preview, ApiDoc, AppData};
use actix_web::middleware::Logger;
use actix_web::{web, App, HttpServer};
use log::info;
use std::{env, io};
use utoipa::OpenApi;
use utoipa_swagger_ui::SwaggerUi;

/// Структура для запуска web-сервера.
pub struct Server {
    bind_address: String,
    workers: usize,
    app_data: AppData,
}

impl Server {
    /// Конструктор сервера.
    ///  - `bind_address` - адрес:порт для прослушивания, например `127.0.0.1:8000`;
    ///  - `log_level` - уровень логирования, например `debug`, по-умолчанию `info`;
    ///  - `workers` - количество потоков для web-сервера, по-умолчанию `2`;
    ///  - `app_data` - заполненная структура с данными для бизнес-логики.
    ///
    /// # Examples
    /// ```no_run
    /// use cutter::prelude::{AppData, StorageMongoDB, Server};
    /// use std::error;
    ///
    /// #[actix_web::main]
    /// async fn main() -> Result<(), Box<dyn error::Error>> {
    ///     let app_data = AppData::new(Box::new(StorageMongoDB::new(
    ///         "mongodb", "mongodb://user:password@localhost:27017/my_db".to_string(), 50)
    ///         .await?
    ///     ));
    ///     Server::new("127.0.0.1:8000".to_string(), "debug".to_string(), 5, app_data)
    ///         .start()
    ///         .await?;
    ///
    ///     Ok(())
    /// }
    /// ```
    pub fn new(bind_address: String, log_level: String, workers: usize, app_data: AppData) -> Self {
        env::set_var("RUST_LOG", log_level);
        env_logger::init();

        Self {
            bind_address,
            workers,
            app_data,
        }
    }

    /// Запуск сервера после конфигурации.
    ///
    /// # Errors
    /// Ошибка [`Err`] может быть при запуске сервера с некорректной привязкой к адресу:порту.
    pub async fn start(self) -> io::Result<()> {
        info!(
            "Using storage: '{}' with LRU size: '{}'",
            self.app_data.storage.name(),
            self.app_data.storage.lru_size()
        );
        info!("Server is starting on: {} ...", self.bind_address);

        let data = web::Data::new(self.app_data);

        HttpServer::new(move || {
            App::new()
                .app_data(web::Data::clone(&data))
                .wrap(Logger::new(
                    "%{r}a '%r' %s %b '%{Referer}i' '%{User-Agent}i' %D ms",
                ))
                .service(get_health_check)
                .service(get_image_preview)
                .service(
                    SwaggerUi::new("/swagger-ui/{_:.*}")
                        .url("/api-docs/openapi.json", ApiDoc::openapi()),
                )
                .service(web::redirect("/", "/swagger-ui/"))
        })
        .workers(self.workers)
        .bind(&self.bind_address)?
        .run()
        .await?;

        Ok(())
    }
}
