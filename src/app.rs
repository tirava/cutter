//! Модуль бизнес-логики приложения.
use crate::prelude::{CutterError, Storage};
use image::imageops;
use log::debug;
use std::io::Cursor;

/// Структура для хранения изображения в кэше.
#[derive(Clone)]
pub struct CachedImage {
    pub body: Vec<u8>,
    pub format: image::ImageFormat,
}

/// Основная структура для работы бизнес-логики.
///  - `storage` - хранилище изображений (память, диск, MongoDB);
///  - `http_client` - HTTP-клиент для получения исходного изображения.
pub struct AppData {
    pub storage: Box<dyn Storage + Send + Sync>,
    http_client: reqwest::Client,
}

impl AppData {
    /// Конструктор бизнес-логики.
    ///  - `storage` - хранилище изображений, реализующее трейт ([`Storage`]).
    ///
    /// # Examples
    /// ```no_run
    /// use cutter::prelude::{AppData, StorageDisk};
    /// use std::error;
    ///
    /// #[actix_web::main]
    /// async fn main() -> Result<(), Box<dyn error::Error>> {
    ///     let _app_data = AppData::new(Box::new(StorageDisk::new("disk", "./cache_files".to_string(), 25)));
    ///
    ///     Ok(())
    /// }
    /// ```
    pub fn new(storage: Box<dyn Storage + Send + Sync>) -> Self {
        Self {
            storage,
            http_client: reqwest::Client::new(),
        }
    }

    /// Основной метод с логикой получения изображения, его нарезки и кэширования.
    ///  - `url` - полный URL-адрес изображения;
    ///  - `width` и `height` - макс. ширина и высота изображение после обрезки (одна из сторон может быть меньше).
    ///
    /// Результатом работы метода является кэшированное изображение в виде структуры [`CachedImage`].
    ///
    /// # Examples
    /// ```no_run
    /// use cutter::prelude::{AppData, StorageMemory};
    /// use std::error;
    ///
    /// #[actix_web::main]
    /// async fn main() -> Result<(), Box<dyn error::Error>> {
    ///     let app_data = AppData::new(Box::new(StorageMemory::new("memory", 10)));
    ///     let _image = match app_data
    ///         .image_preview("https://www.rust-lang.org/logos/rust-logo-128x128.png".to_string(),
    ///             111, 111)
    ///         .await
    ///     {
    ///         Ok(image) => image.body,
    ///         Err(err) => return Err(err.into()),
    ///     };
    ///
    ///     Ok(())
    /// }
    /// ```
    /// # Errors
    /// Ошибка [`CutterError`] может быть получена в следующих случаях:
    ///  - проблемы при работе с хранилищем кэша;
    ///  - проблемы с получением исходного изображения;
    ///  - проблемы с обрезкой изображения.
    pub async fn image_preview(
        &self,
        url: String,
        width: u32,
        height: u32,
    ) -> Result<CachedImage, CutterError> {
        let url = match url.starts_with("http://") || url.starts_with("https://") {
            true => url,
            false => format!("https://{url}"),
        }
        .to_lowercase();

        let key = format!("{url}_{width}_{height}");
        let cached = self.storage.get(&key).await?;
        if let Some(image) = cached {
            debug!("Using cached image: {url} with size: {width} x {height}");

            return Ok(image);
        }

        debug!("Downloading and cutting image: {url} with size: {width} x {height} ...");

        let orig_image = self.http_client.get(&url).send().await?.bytes().await?;
        let image_format = match image::guess_format(&orig_image) {
            Ok(format) => format,
            Err(err) => {
                return Err(CutterError::from(err));
            }
        };

        let image = image::load_from_memory(&orig_image)?.resize(
            width,
            height,
            imageops::FilterType::Lanczos3,
        );
        let mut new_image: Cursor<Vec<u8>> = Cursor::new(vec![]);
        image.write_to(&mut new_image, image_format)?;

        self.storage
            .insert(
                key,
                CachedImage {
                    body: new_image.get_ref().to_vec(),
                    format: image_format,
                },
            )
            .await?;

        Ok(CachedImage {
            body: new_image.into_inner(),
            format: image_format,
        })
    }
}
