use cutter::prelude::{AppData, Server, StorageDisk, StorageMemory, StorageMongoDB};
use std::{env, error};

const MEMORY: &str = "memory";
const DISK: &str = "disk";
const MONGODB: &str = "mongodb";

#[actix_web::main]
async fn main() -> Result<(), Box<dyn error::Error>> {
    dotenv::dotenv().ok();

    let log_level = env::var("RUST_LOG").unwrap_or("info".to_string());
    let bind_address = env::var("BIND_ADDRESS").unwrap_or("127.0.0.1:8000".to_string());
    let workers = env::var("WORKERS").unwrap_or(2.to_string()).parse()?;
    let lru_size_memory = env::var("STORAGE_MEMORY_LRU_SIZE")
        .unwrap_or(10.to_string())
        .parse()?;
    let lru_size_disk = env::var("STORAGE_DISK_LRU_SIZE")
        .unwrap_or(20.to_string())
        .parse()?;
    let lru_size_mongodb = env::var("STORAGE_MONGODB_LRU_SIZE")
        .unwrap_or(30.to_string())
        .parse()?;
    let storage = env::var("STORAGE").unwrap_or(MEMORY.to_string());
    let disk_path = env::var("STORAGE_DISK_PATH").unwrap_or(".".to_string());
    let mongodb_uri = env::var("STORAGE_MONGODB_URI").unwrap_or("".to_string());

    let app_data = match storage.as_str() {
        MEMORY => AppData::new(Box::new(StorageMemory::new(MEMORY, lru_size_memory))),
        DISK => AppData::new(Box::new(StorageDisk::new(DISK, disk_path, lru_size_disk))),
        MONGODB => AppData::new(Box::new(
            StorageMongoDB::new(MONGODB, mongodb_uri, lru_size_mongodb).await?,
        )),
        _ => return Err(format!("Unknown storage: {storage}").into()),
    };

    Server::new(bind_address, log_level, workers, app_data)
        .start()
        .await?;

    Ok(())
}
