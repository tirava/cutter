mod app;
mod error;
mod handler;
mod server;
mod storage;

pub mod prelude {
    pub use crate::{
        app::{AppData, CachedImage},
        error::CutterError,
        handler::{get_health_check, get_image_preview, ApiDoc},
        server::Server,
        storage::prelude::*,
    };
}
