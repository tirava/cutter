//! Модуль хранилища кэша изображений.
use crate::prelude::{CachedImage, CutterError};
use async_trait::async_trait;
use std::hash::{DefaultHasher, Hash, Hasher};

pub(crate) mod disk;
pub(crate) mod memory;
pub(crate) mod mongodb;

pub mod prelude {
    pub use super::disk::StorageDisk;
    pub use super::memory::StorageMemory;
    pub use super::mongodb::StorageMongoDB;
    pub use super::Storage;
}

/// Интерфейс хранилища кэша изображений.
///  - [`memory::StorageMemory`] - хранилище в оперативной памяти;
///  - [`disk::StorageDisk`] - хранилище на диске;
///  - [`mongodb::StorageMongoDB`] - хранилище в MongoDB.
#[async_trait]
pub trait Storage {
    /// Возвращает имя хранилища (задается в конструкторе).
    fn name(&self) -> &str;

    /// Возвращает размер LRU-кэша (в количестве элементов).
    fn lru_size(&self) -> usize;

    /// Возвращает оставшийся размер LRU-кэша (в количестве элементов).
    async fn lru_left(&self) -> usize;

    /// Хэширует URL изображения и возвращает имя файла (для диска) или ключа (для БД).
    fn hash(&self, key: &str) -> String {
        let mut hasher = DefaultHasher::new();
        key.hash(&mut hasher);
        hasher.finish().to_string()
    }

    /// Возвращает изображение по ключу из LRU-кэша.
    /// Ключ - полный URL изображения + размеры, в размерах `/` (прямые слеши) заменяются на `_` (подчеркивания).
    ///
    /// Если изображение не найдено, возвращает `None`.
    /// # Examples
    /// ```no_run
    /// use cutter::prelude::{AppData, StorageMemory};
    /// use std::error;
    ///
    /// #[actix_web::main]
    /// async fn main() -> Result<(), Box<dyn error::Error>> {
    ///     let app_data = AppData::new(Box::new(StorageMemory::new("memory", 10)));
    ///
    ///     let width = 111;
    ///     let height = 222;
    ///     let url = "https://www.rust-lang.org/logos/rust-logo-128x128.png";
    ///     let key = format!("{url}_{width}_{height}");
    ///
    ///     let cached = app_data.storage.get(&key).await?;
    ///     if let Some(image) = cached {
    ///         println!("Found cached image: {url} with size: {width} x {height}");
    ///     }
    ///
    ///     Ok(())
    /// }
    /// ```
    /// # Errors
    /// Ошибка [`CutterError`] может быть получена при проблемах с хранилищем кэша.
    async fn get(&self, key: &str) -> Result<Option<CachedImage>, CutterError>;

    /// Сохраняет изображение по ключу в LRU-кэше.
    /// Ключ - полный URL изображения + размеры, в размерах `/` (прямые слеши) заменяются на `_` (подчеркивания).
    /// # Examples
    /// ```no_run
    /// use cutter::prelude::{AppData, CachedImage, StorageMemory};
    /// use std::error;
    /// use std::io::Cursor;
    ///
    /// #[actix_web::main]
    /// async fn main() -> Result<(), Box<dyn error::Error>> {
    ///     let app_data = AppData::new(Box::new(StorageMemory::new("memory", 10)));
    ///
    ///     let width = 111;
    ///     let height = 222;
    ///     let url = "https://www.rust-lang.org/logos/rust-logo-128x128.png";
    ///     let key = format!("{url}_{width}_{height}");
    ///
    ///     let mut resized_image: Cursor<Vec<u8>> = Cursor::new(vec![]);
    ///     let image_format = image::ImageFormat::Png;
    ///
    ///     // let image = image::load_from_memory(...)?.resize(...);
    ///     // image.write_to(&mut resized_image, image_format)?;
    ///
    ///     let cached_image = CachedImage {
    ///                  body: resized_image.get_ref().to_vec(),
    ///                  format: image_format,
    ///                 };
    ///
    ///     app_data.storage.insert(key, cached_image).await?;
    ///
    ///     Ok(())
    /// }
    /// ```
    /// # Errors
    /// Ошибка [`CutterError`] может быть получена при проблемах с хранилищем кэша.
    async fn insert(&self, key: String, image: CachedImage) -> Result<(), CutterError>;
}
