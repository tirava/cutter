//! Модуль реализации хранилища кэша изображений на диске.
use crate::app::CachedImage;
use crate::error::CutterError;
use crate::prelude::Storage;
use async_trait::async_trait;
use log::{debug, error};
use moka::future::{Cache, FutureExt};
use moka::notification::ListenerFuture;
use std::path::PathBuf;
use std::sync::Arc;
use tokio::fs;
use tokio::io::{AsyncReadExt, AsyncWriteExt};

/// Хранилище кэша изображений на диске.
///  - `name` - символическое имя хранилища (задается в конструкторе).
///  - `lru_size` - размер кэша (количество кэшируемых изображений).
///  - `base_path` - базовый путь к каталогу, в котором хранятся кэшируемые изображения.
///  - `cache` - LRU-кэш изображений на диске.
pub struct StorageDisk {
    name: String,
    lru_size: usize,
    base_path: String,
    cache: Cache<String, String>,
}

impl StorageDisk {
    /// Конструктор хранилища кэша изображений на диске.
    ///  - `name` - символическое имя хранилища.
    ///  - `base_path` - базовый путь к каталогу, в котором хранятся кэшируемые изображения.
    ///  - `lru_size` - размер кэша (количество кэшируемых изображений).
    ///
    /// # Examples
    /// ```no_run
    /// use cutter::prelude::StorageDisk;
    ///
    /// fn __main() {
    ///    let storage = StorageDisk::new("disk", "./cache_files".to_string(), 10);
    /// }
    /// ```
    pub fn new(name: &str, base_path: String, lru_size: usize) -> Self {
        let base_path_clone = base_path.clone();

        let eviction_listener = move |key: Arc<String>,
                                      file_name: String,
                                      cause|
              -> ListenerFuture {
            let full_path = PathBuf::from(&base_path).join(file_name);
            debug!(
                "Cached image file will be deleted: {key:?}, {:?}, cause - {cause:?}",
                &full_path
            );

            async move {
                if let Err(err) = fs::remove_file(&full_path).await {
                    error!("Failed to remove image file: {key:?}, {full_path:?}, cause - {err:?}");
                }
            }
            .boxed()
        };

        Self {
            name: name.to_string(),
            lru_size,
            base_path: base_path_clone,
            cache: Cache::builder()
                .async_eviction_listener(eviction_listener)
                .max_capacity(lru_size as u64)
                .build(),
        }
    }

    /// Вспомогательный метод для включения в кэш изображений, которые уже есть на диске.
    async fn cache_exists_file(&self, key: String) -> (String, bool) {
        let file_name = self.hash(&key);
        let full_path = PathBuf::from(&self.base_path).join(&file_name);

        match full_path.exists() {
            true => {
                self.cache.insert(key, file_name.clone()).await;
                (file_name, true)
            }
            false => (file_name, false),
        }
    }
}

#[async_trait]
impl Storage for StorageDisk {
    fn name(&self) -> &str {
        &self.name
    }

    fn lru_size(&self) -> usize {
        self.lru_size
    }

    async fn lru_left(&self) -> usize {
        self.cache.run_pending_tasks().await;

        self.lru_size - self.cache.entry_count() as usize
    }

    async fn get(&self, key: &str) -> Result<Option<CachedImage>, CutterError> {
        let cached_file_name = match self.cache.get(key).await {
            Some(file_name) => file_name,
            None => match self.cache_exists_file(key.to_string()).await {
                (file_name, true) => file_name,
                (_, false) => return Ok(None),
            },
        };

        let full_path = PathBuf::from(&self.base_path).join(&cached_file_name);
        let mut file = match fs::File::open(&full_path).await {
            Ok(file) => file,
            Err(err) => {
                error!("Failed to open image file: {key:?}, {full_path:?}, cause - {err:?}");
                self.cache.invalidate(key).await;
                return Ok(None);
            }
        };

        let mut buffer = Vec::new();
        file.read_to_end(&mut buffer).await?;

        let image = CachedImage {
            format: image::guess_format(&buffer)?,
            body: buffer,
        };

        Ok(Some(image))
    }

    async fn insert(&self, key: String, image: CachedImage) -> Result<(), CutterError> {
        let file_name = match self.cache_exists_file(key.clone()).await {
            (_, true) => return Ok(()),
            (file_name, false) => file_name,
        };

        let full_path = PathBuf::from(&self.base_path).join(&file_name);
        let mut file = fs::File::create(full_path).await?;
        file.write_all(&image.body).await?;

        self.cache.insert(key, file_name).await;
        self.cache.run_pending_tasks().await;

        Ok(())
    }
}
