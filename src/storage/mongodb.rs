//! Модуль реализации хранилища кэша изображений в MongoDB.
use crate::app::CachedImage;
use crate::error::CutterError;
use crate::prelude::Storage;
use async_trait::async_trait;
use log::{debug, error};
use moka::future::{Cache, FutureExt};
use moka::notification::ListenerFuture;
use mongodb::bson::spec::BinarySubtype;
use mongodb::bson::{doc, Binary};
use mongodb::{Client, Collection};
use serde::{Deserialize, Serialize};
use std::sync::Arc;

/// Хранилище кэша изображений в MongoDB.
///  - `name` - символическое имя хранилища (задается в конструкторе).
///  - `lru_size` - размер кэша (количество кэшируемых изображений).
///  - `cache` - LRU-кэш изображений в MongoDB.
///  - `collection_images` - хэндл на коллекцию изображений в MongoDB (инициализируется в конструкторе).
pub struct StorageMongoDB {
    name: String,
    lru_size: usize,
    cache: Cache<String, String>,
    collection_images: Collection<CollectionImage>,
}

#[derive(Serialize, Deserialize)]
struct CollectionImage {
    name: String,
    body: Binary,
}

impl StorageMongoDB {
    /// Конструктор хранилища кэша изображений в MongoDB.
    ///  - `name` - символическое имя хранилища.
    ///  - `uri` - строка подключения к MongoDB (mongodb://user:password@localhost:27017/my_db).
    ///  - `lru_size` - размер кэша (количество кэшируемых изображений).
    ///
    /// # Examples
    /// ```no_run
    /// use cutter::prelude::StorageMongoDB;
    ///
    /// fn __main() {
    ///    let storage = StorageMongoDB::new("mongodb", "mongodb://user:password@localhost:27017/my_db".to_string(), 100);
    /// }
    /// ```
    pub async fn new(name: &str, uri: String, lru_size: usize) -> Result<Self, CutterError> {
        let client = Client::with_uri_str(&uri).await?;
        let db = match client.default_database() {
            Some(db) => db,
            None => {
                return Err(CutterError::OtherError(
                    "No default database found in MongoDB uri string".to_string(),
                ))
            }
        };

        let collection_images = db.collection("images");

        let collection_images_clone = collection_images.clone();
        let eviction_listener = move |key: Arc<String>,
                                      image_name: String,
                                      cause|
              -> ListenerFuture {
            debug!(
                    "Cached image in MongoDB will be deleted: {key:?}, {image_name:?}, cause - {cause:?}"
                );

            let collection_images_clone = collection_images_clone.clone();
            async move {
                if let Err(err) = collection_images_clone.delete_one(doc! {"name": &image_name}).await {
                    error!("Failed to remove image in MongoDB: {key:?}, {image_name:?}, cause - {err:?}");
                }
            }
            .boxed()
        };

        Ok(Self {
            name: name.to_string(),
            lru_size,
            cache: Cache::builder()
                .async_eviction_listener(eviction_listener)
                .max_capacity(lru_size as u64)
                .build(),
            collection_images,
        })
    }

    /// Вспомогательный метод для включения в кэш изображений, которые уже есть в MongoDB.
    async fn cache_exists_image(&self, key: String) -> Result<(String, bool), CutterError> {
        let image_name = self.hash(&key);

        if self
            .collection_images
            .count_documents(doc! {"name": &image_name})
            .await?
            > 0
        {
            self.cache.insert(key, image_name.clone()).await;
            return Ok((image_name, true));
        }

        Ok((image_name, false))
    }
}

#[async_trait]
impl Storage for StorageMongoDB {
    fn name(&self) -> &str {
        &self.name
    }

    fn lru_size(&self) -> usize {
        self.lru_size
    }

    async fn lru_left(&self) -> usize {
        self.cache.run_pending_tasks().await;

        self.lru_size - self.cache.entry_count() as usize
    }

    async fn get(&self, key: &str) -> Result<Option<CachedImage>, CutterError> {
        let cached_image_name = match self.cache.get(key).await {
            Some(image_name) => image_name,
            None => match self.cache_exists_image(key.to_string()).await? {
                (image_name, true) => image_name,
                (_, false) => return Ok(None),
            },
        };

        let collection_image = match self
            .collection_images
            .find_one(doc! {"name": &cached_image_name})
            .await?
        {
            Some(image) => image,
            None => {
                error!("Failed to read image from MongoDB: {key:?}, {cached_image_name:?}");
                self.cache.invalidate(key).await;
                return Ok(None);
            }
        };

        let image = CachedImage {
            format: image::guess_format(&collection_image.body.bytes)?,
            body: collection_image.body.bytes,
        };

        Ok(Some(image))
    }

    async fn insert(&self, key: String, image: CachedImage) -> Result<(), CutterError> {
        let image_name = match self.cache_exists_image(key.clone()).await? {
            (_, true) => return Ok(()),
            (file_name, false) => file_name,
        };

        self.collection_images
            .insert_one(CollectionImage {
                name: image_name.clone(),
                body: Binary {
                    subtype: BinarySubtype::Generic,
                    bytes: image.body,
                },
            })
            .await?;

        self.cache.insert(key, image_name).await;
        self.cache.run_pending_tasks().await;

        Ok(())
    }
}
