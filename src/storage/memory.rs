//! Модуль реализации хранилища кэша изображений в оперативной памяти.
use crate::app::CachedImage;
use crate::error::CutterError;
use crate::prelude::Storage;
use async_trait::async_trait;
use log::debug;
use moka::{future::Cache, future::FutureExt, notification::ListenerFuture};

/// Хранилище кэша изображений в оперативной памяти.
///  - `name` - символическое имя хранилища (задается в конструкторе).
///  - `lru_size` - размер кэша (количество кэшируемых изображений).
///  - `cache` - LRU-кэш изображений в оперативной памяти.
pub struct StorageMemory {
    name: String,
    lru_size: usize,
    cache: Cache<String, CachedImage>,
}

impl StorageMemory {
    /// Конструктор хранилища кэша изображений в оперативной памяти.
    ///  - `name` - символическое имя хранилища.
    ///  - `lru_size` - размер кэша (количество кэшируемых изображений).
    ///
    /// # Examples
    /// ```no_run
    /// use cutter::prelude::StorageMemory;
    ///
    /// fn __main() {
    ///    let storage = StorageMemory::new("memory", 10);
    /// }
    /// ```
    pub fn new(name: &str, lru_size: usize) -> Self {
        let eviction_listener = move |key, _image: CachedImage, cause| -> ListenerFuture {
            debug!("Cached image in memory will be deleted: {key:?}, cause - {cause:?}");

            async move {}.boxed()
        };

        Self {
            name: name.to_string(),
            lru_size,
            cache: Cache::builder()
                .max_capacity(lru_size as u64)
                .async_eviction_listener(eviction_listener)
                .build(),
        }
    }
}

#[async_trait]
impl Storage for StorageMemory {
    fn name(&self) -> &str {
        &self.name
    }

    fn lru_size(&self) -> usize {
        self.lru_size
    }

    async fn lru_left(&self) -> usize {
        self.cache.run_pending_tasks().await;

        self.lru_size - self.cache.entry_count() as usize
    }

    async fn get(&self, key: &str) -> Result<Option<CachedImage>, CutterError> {
        Ok(self.cache.get(key).await)
    }

    async fn insert(&self, key: String, image: CachedImage) -> Result<(), CutterError> {
        self.cache
            .insert(
                key,
                CachedImage {
                    body: image.body,
                    format: image.format,
                },
            )
            .await;
        self.cache.run_pending_tasks().await;

        Ok(())
    }
}
