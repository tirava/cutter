//! Модуль обработки http-запросов.
use crate::prelude::AppData;
use actix_web::{get, web, HttpResponse, Responder};
use log::{error, warn};
use serde::{Deserialize, Serialize};
use utoipa::{IntoParams, OpenApi, ToSchema};
use validator::Validate;

/// Базовая Swagger-UI API документация.
#[derive(OpenApi)]
#[openapi(
    paths(
        get_health_check,
        get_image_preview
    ),
    components(
        schemas(Response)
    ),
    tags(
        (name = "Cutter REST API", description = "Cutter API for cutting images")
    ),
)]
pub struct ApiDoc;

/// Ответ сервера при наличии ошибки.
#[derive(Serialize, ToSchema)]
struct Response {
    status: &'static str,
    message: String,
}

/// Хэндлер для проверки состояния сервера.
#[utoipa::path(
    get,
    tag = "Health Check",
    responses(
        (status = 200, description = "OK"),
    )
)]
#[get("/healthz")]
async fn get_health_check() -> impl Responder {
    HttpResponse::Ok()
}

/// Структура для запроса превью изображения.
///  - `width` и `height` - ширина и высота;
///  - `tail` - полный URL изображения.
#[derive(Serialize, Deserialize, Validate, IntoParams)]
struct ImagePreviewRequest {
    /// image url
    #[validate(length(min = 1, max = 500, message = "URL is required"))]
    tail: String,
    #[validate(range(min = 1, max = 5000))]
    width: u32,
    #[validate(range(min = 1, max = 5000))]
    height: u32,
}

/// Основной хэндлер для запроса превью изображения.
#[utoipa::path(
    get,
    tag = "Cut and preview image",
    params(
        ImagePreviewRequest
    ),
    responses(
        (status = 200, description = "OK"),
		(status = 400, description = "Validation Error", body = Response),
		(status = 500, description = "Internal Server Error", body = Response),
    )
)]
#[get("/image-preview/{width}/{height}/{tail:.*}")]
async fn get_image_preview(
    path: web::Path<ImagePreviewRequest>,
    app_data: web::Data<AppData>,
) -> impl Responder {
    let path_params: ImagePreviewRequest = path.into_inner();
    if let Err(err) = path_params.validate() {
        warn!("{:?}", err);
        return HttpResponse::BadRequest().json(Response {
            status: "request error",
            message: err.to_string().replace('"', "'"),
        });
    }

    match app_data
        .image_preview(path_params.tail, path_params.width, path_params.height)
        .await
    {
        Ok(image) => HttpResponse::Ok()
            .content_type(image.format.to_mime_type())
            .body(image.body),
        Err(err) => {
            error!("{:?}", err);
            HttpResponse::InternalServerError().json(Response {
                status: "server error",
                message: err.to_string(),
            })
        }
    }
}
