#!/usr/bin/env bash

ENV=$1
CMD=$2

DC_CMD="docker-compose -f docker-compose.yml -f docker-compose.${ENV}.yml"

case ${CMD} in
up)
  git pull
  ${DC_CMD} up -d --build --remove-orphans
  ;;
down)
  ${DC_CMD} down
  ;;
logs)
  ${DC_CMD} logs -f "$3"
  ;;
ps)
  ${DC_CMD} ps
  ;;
restart)
  ${DC_CMD} restart "$3"
  ;;
stop)
  ${DC_CMD} stop "$3"
  ;;
start)
  ${DC_CMD} start "$3"
  ;;
exec)
  ${DC_CMD} exec "$3" "$4"
  ;;
*)
  echo "Usage ./ctr.sh [ENV] [CMD] [OPTIONS]"
  ;;
esac
