.PHONY: up down logs test

DEPLOY?=debug
DC_CMD=docker-compose -f docker-compose.yml -f docker-compose.$(DEPLOY).yml

up:
	$(DC_CMD) up -d --build --remove-orphans

down:
	$(DC_CMD) down
	
logs:
	$(DC_CMD) logs -f api

test:
	set -e ;\
    docker-compose -f docker-compose.test.yml up --no-start --build ;\
    test_status_code=0 ;\
    docker-compose -f docker-compose.test.yml run cutter-integration-tests || test_status_code=$$? ;\
    docker-compose -f docker-compose.test.yml down ;\
    exit $$test_status_code ;\