use actix_web::web::Bytes;
use cutter::prelude::{AppData, CutterError, StorageDisk, StorageMemory};
use httpmock::prelude::*;
use std::path::PathBuf;
use tokio::{fs, io::AsyncReadExt};

const MEMORY: &str = "memory";
const DISK: &str = "disk";
const DISK_PATH: &str = "./tests/examples";
// const MONGODB: &str = "mongodb";
const LRU_SIZE: usize = 1;

#[actix_web::test]
async fn memory_resize_jpg_111_test() {
    test_helper(MEMORY, "/images/1.jpg", 111, 111, "image1_111.jpg").await
}

#[actix_web::test]
async fn disk_resize_jpg_111_test() {
    test_helper(DISK, "/images/1.jpg", 111, 111, "*hash*").await
}

#[actix_web::test]
async fn memory_resize_jpeg_333_test() {
    test_helper(MEMORY, "/images/2", 333, 333, "image2_333.jpeg").await
}

#[actix_web::test]
async fn memory_resize_png_555_test() {
    test_helper(MEMORY, "/images/3", 555, 555, "image3_555.png").await
}

async fn test_helper(storage: &str, url: &str, width: u32, height: u32, expected_image_name: &str) {
    let server = source_image_server().await.unwrap();

    let app_data = match storage {
        MEMORY => AppData::new(Box::new(StorageMemory::new(MEMORY, LRU_SIZE))),
        DISK => AppData::new(Box::new(StorageDisk::new(
            DISK,
            DISK_PATH.to_string(),
            LRU_SIZE,
        ))),
        // todo
        // MONGODB => AppData::new(Box::new(
        //     StorageMongoDB::new(MONGODB, "mongodb_uri".to_string(), lru_size).await?,
        // )),
        _ => panic!("unknown storage"),
    };

    let image_path = server.url(url);
    let filename = match storage {
        DISK => app_data
            .storage
            .hash(format!("{image_path}_{width}_{height}").as_str()),
        _ => expected_image_name.to_string(),
    };

    // проверяем, что обрезанная картинка соответствует заранее подготовленному тестовому образцу
    let resized = app_data
        .image_preview(image_path.clone(), width, height)
        .await
        .unwrap();
    let path = PathBuf::from(DISK_PATH).join(filename);
    let expected = read_image_from_disk(path).await.unwrap();
    assert_eq!(resized.body, expected);

    // проверяем, что оставшийся размер кэша уменьшился на 1
    let lru_left = app_data.storage.lru_left().await;
    assert_eq!(lru_left, LRU_SIZE - 1);

    // проверяем, что картинка есть в кэше и что она соответствует заранее подготовленному тестовому образцу
    let key = format!("{image_path}_{width}_{height}");
    let cached_image = app_data.storage.get(key.as_str()).await.unwrap().unwrap();
    assert_eq!(cached_image.body, expected);

    // для дальнейших тестов размер кэша должен быть 1, чтобы уменьшить размер тестового кода
    if LRU_SIZE != 1 {
        return;
    }

    // обрезаем эту же картинку, но с увеличенным размером
    // в кэш она не должна попасть, т.к. предыдущая уже 1 раз читалась из кэша, а эта еще ни разу
    app_data
        .image_preview(image_path.clone(), width + 1, height + 1)
        .await
        .unwrap();
    let key1 = format!("{image_path}_{}_{}", width + 1, height + 1);
    assert!(app_data.storage.get(key.as_str()).await.unwrap().is_some());
    assert!(app_data.storage.get(key1.as_str()).await.unwrap().is_none());

    // еще раз обрезаем эту же картинку с увеличенным размером
    // в кэш она должна попасть, а вот старая удалится
    let resized1 = app_data
        .image_preview(image_path.clone(), width + 1, height + 1)
        .await
        .unwrap();
    assert!(app_data.storage.get(key.as_str()).await.unwrap().is_none());
    assert!(app_data.storage.get(key1.as_str()).await.unwrap().is_some());

    // проверяем, что новая картинка есть в кэше и что она соответствует обрезанной
    let cached_image1 = app_data.storage.get(key1.as_str()).await.unwrap().unwrap();
    assert_eq!(cached_image1.body, resized1.body);
}

async fn source_image_server() -> Result<MockServer, CutterError> {
    let base_path = PathBuf::from(DISK_PATH);
    let server = MockServer::start();

    let buffer1 = read_image_from_disk(base_path.join("image1.jpg")).await?;
    server.mock(|when, then| {
        when.method("GET").path("/images/1.jpg");
        then.status(200).body(buffer1);
    });

    let buffer2 = read_image_from_disk(base_path.join("image2.jpeg")).await?;
    server.mock(|when, then| {
        when.method("GET").path("/images/2");
        then.status(200).body(buffer2);
    });

    let buffer3 = read_image_from_disk(base_path.join("image3.png")).await?;
    server.mock(|when, then| {
        when.method("GET").path("/images/3");
        then.status(200).body(buffer3);
    });

    Ok(server)
}

async fn read_image_from_disk(path: PathBuf) -> Result<Bytes, CutterError> {
    let mut file = fs::File::open(path).await?;
    let mut buffer = Vec::new();
    file.read_to_end(&mut buffer).await?;

    Ok(buffer.into())
}
