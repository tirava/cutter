FROM rust:1.80.0 as builder

RUN USER=root cargo new --bin cutter
WORKDIR /cutter

RUN apt update && apt install -y pkg-config libssl-dev

#COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

RUN cargo build --release
RUN rm src/*.rs

COPY ./src ./src

RUN rm ./target/release/deps/cutter*
RUN cargo build --release

FROM debian

RUN apt update && apt install -y pkg-config libssl-dev ca-certificates

COPY --from=builder /cutter/target/release/cutter .
RUN mkdir "/cache_files"

EXPOSE 8080

CMD ["./cutter"]
